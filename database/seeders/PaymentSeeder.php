<?php

namespace Database\Seeders;

use App\Models\Currency;
use App\Models\Merchant;
use App\Models\Payment;

use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $merchants = Merchant::all();
        $currency = Currency::query()->first();

        foreach ($merchants as $merchant) {
            Payment::factory()->count(5)->for($currency)->for($merchant)->create();
        }
    }
}
