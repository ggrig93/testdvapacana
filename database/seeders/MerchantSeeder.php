<?php

namespace Database\Seeders;

use App\Enums\PaymentStatuses;
use App\Models\Currency;
use App\Models\Merchant;
use App\Models\Payment;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class MerchantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currency = Currency::query()->first();

        Merchant::insert([
            [
                'name' => 'Gateway 1',
                'currency_id' => $currency->id,
                'payment_limit' => 100,
                'merchant_id' => 6,
                'key' => 'KaTf5tZYHx4v7pgZ',
            ],
            [
                'name' => 'Gateway 2',
                'currency_id' => $currency->id,
                'payment_limit' => 100,
                'merchant_id' => 816,
                'key' => 'rTaasVHeteGbhwBx',
            ]
        ]);
    }
}
