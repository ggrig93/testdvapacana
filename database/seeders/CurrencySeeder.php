<?php

namespace Database\Seeders;

use App\Enums\PaymentStatuses;
use App\Models\Currency;
use App\Models\Merchant;
use App\Models\Payment;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::create(['name' => 'USD']);
    }
}
