<?php

namespace Database\Factories;

use App\Enums\PaymentStatuses;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Payment>
 */
class PaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $amount = fake()->numberBetween(1000, 100000);
        $amount_paid = fake()->numberBetween(10000, $amount);

        return [
            'status' => Arr::random([PaymentStatuses::NEW, PaymentStatuses::PENDING]),
            'amount' =>  $amount,
            'amount_paid' => $amount_paid,
            'callback_url' => fake()->url()
        ];
    }
}
