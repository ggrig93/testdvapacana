<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'status' => $this->status,
            'currency' => $this->currency->name,
            'amount' => $this->amount,
            'amount_paid' => $this->amount_paid,
            'amount_cent' => $this->amount_cent,
            'amount_paid_cent' => $this->amount_paid_cent,
        ];
    }
}
