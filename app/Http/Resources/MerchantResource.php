<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MerchantResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            $this->mergeWhen( $this->payment_limit, [
                'payment_limit' => $this->payment_limit,
            ]),
            'payment_count' => $this->payment_count,
            'currency' => $this->currency->name,
        ];
    }
}
