<?php

namespace App\Http\Controllers;

use App\Http\Resources\PaymentResource;
use App\Models\Payment;
use App\Services\PaymentGatewayProcessor;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class PaymentController extends Controller
{
    /**
     * @param Payment $payment
     * @return JsonResponse
     */
    public function updateStatus( Payment $payment): JsonResponse
    {
        PaymentGatewayProcessor::process($payment);

       return response()->json(PaymentResource::make($payment), Response::HTTP_OK);
    }
}
