<?php

namespace App\Http\Controllers;

use App\Http\Requests\MerchantLimitRequest;
use App\Http\Resources\MerchantResource;
use App\Models\Merchant;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class MerchantController extends Controller
{
    /**
     * @param MerchantLimitRequest $request
     * @param Merchant $merchant
     * @return JsonResponse
     */
    public function limit(MerchantLimitRequest $request, Merchant $merchant): JsonResponse
    {
        $merchant->update(['limit', $request->limit]);
        $merchant->load('currency');

        return response()->json(MerchantResource::make($merchant), Response::HTTP_OK);
    }
}
