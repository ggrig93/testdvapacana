<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 *
 */
class Merchant extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'currency_id',
        'payment_limit'
    ];

    protected $with = ['currency'];


    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return string
     */
    public function getPaymentProcessorAttribute(): string
    {
        return "App/Services/Gateway{$this->merchant_id}.php";
    }
}
