<?php

namespace App\Models;

use App\Utils\CurrencyToCent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'status',
        'amount',
        'amount_paid',
        'callback_url',
        'currency_id',
        'merchant_id'
    ];

    protected $with = ['currency'];

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
       return $this->belongsTo(Currency::class);
    }

    /**
     * @return BelongsTo
     */
    public function merchant(): BelongsTo
    {
       return $this->belongsTo(Currency::class);
    }

    /**
     * @return float
     */
    public function getAmountCentAttribute(): float
    {
        return CurrencyToCent::convert($this->currency?->name, $this->ammount);

    }

    /**
     * @return float
     */
    public function getAmountPaidCentAttribute(): float
    {
        return CurrencyToCent::convert($this->currency?->name, $this->ammount_paid);
    }
}
