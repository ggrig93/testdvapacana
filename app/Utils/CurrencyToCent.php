<?php

namespace App\Utils;

final class CurrencyToCent
{

    /**
     * @param string|null $currency
     * @param float $amount
     * @return float
     */
    public static function convert(?string $currency, float $amount): float
    {
        switch ($currency) {
            case 'USD':
                $amount *= 100;
                break;
        }

        return $amount;
    }
}
