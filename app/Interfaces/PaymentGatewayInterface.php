<?php

namespace App\Interfaces;

use App\Models\Payment;

interface PaymentGatewayInterface
{
    /**
     * @param Payment $model
     * @return mixed
     */
    public static function process(Payment $model);

    /**
     * @param array $params
     * @param string $key
     * @return mixed
     */
    public static function getSign(array $params, string $key);
}
