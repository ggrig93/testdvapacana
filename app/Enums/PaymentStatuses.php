<?php

namespace App\Enums;

use App\Traits\EnumToArray;

enum PaymentStatuses: string
{
   use EnumToArray;

    case NEW = 'new';
    case PENDING = 'pending';
    case COMPLETED = 'completed';
    case EXPIRED = 'expired';
    case REJECTED = 'rejected';

}
