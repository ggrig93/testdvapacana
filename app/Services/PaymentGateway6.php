<?php

namespace App\Services;

use App\Interfaces\PaymentGatewayInterface;
use App\Models\Payment;
use Illuminate\Support\Facades\Http;

class PaymentGateway6 implements PaymentGatewayInterface
{
    /**
     * @param Payment $payment
     * @return mixed
     */
    public static function process(Payment $payment): mixed
    {
        $params = [
            "merchant_id" => $payment->merchant_id,
            "payment_id" => $payment->id,
            "status" => $payment->status,
            "amount" => $payment->amount,
            "amount_paid" => $payment->amount_paid,
            "timestamp" => now(),
        ];


        $params['sign'] =  self::getSign($params, $payment->key);
        $response = Http::withBody(json_encode($params), 'application/json')
            ->post($payment->callback_url);

        return $response->json();
    }

    /**
     * @param array $params
     * @param string $key
     * @return string
     */
    public static function getSign(array $params, string $key): string
    {
        ksort($params);

        return hash('sha256', implode(':', array_keys($params)) . $key);
    }

}
