<?php

namespace App\Services;

use App\Interfaces\PaymentGatewayInterface;
use Illuminate\Support\Str;
use App\Models\Payment;
use Illuminate\Support\Facades\Http;

class PaymentGateway816 implements PaymentGatewayInterface
{
    /**
     * @param Payment $payment
     * @return mixed
     */
    public static function process(Payment $payment): mixed
    {
        $params = [
            "project" => $payment->merchant_id,
            "invoice" => $payment->id,
            "status" => $payment->status,
            "amount" => $payment->amount,
            "amount_paid" => $payment->amount_paid,
            "timestamp" => now(),
            "rand" => Str::random(8)
        ];

        $sign = self::getSign($params, $payment->key);

        $response = Http::withBody(json_encode($params), 'multipart/form-data')->withHeaders([
            'Authorization' => $sign
        ])->post($payment->callback_url);

        return $response->json();
    }

    /**
     * @param array $params
     * @param string $key
     * @return string
     */
    public static function getSign(array $params, string $key): string
    {
        ksort($params);

        return md5(implode('.', array_keys($params)) . $key);
    }
}
