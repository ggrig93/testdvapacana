<?php

namespace App\Services;

use App\Interfaces\PaymentGatewayInterface;
use App\Models\Payment;

class PaymentGatewayProcessor
{
    /**
     * @param Payment $payment
     * @return PaymentGatewayInterface
     */
    public static function process(Payment $payment): PaymentGatewayInterface
    {
        return new $payment->merchant->payment_processor($payment);
    }

}
